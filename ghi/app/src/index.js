import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadHats(){
  //const shoesResponse = await fetch('http://localhost:8080/api/shoes/');
  try{
    const hatsResponse = await fetch('http://localhost:8090/api/hats/');

    if(hatsResponse.ok) {
      const hatData = await hatsResponse.json()
      root.render(
        <React.StrictMode>
          <App hats={hatData} />
        </React.StrictMode>
      );
    } else {
      console.error(hatsResponse)
    }
  } catch (error) {
    console.log("error", error)
  }
}
loadHats()
