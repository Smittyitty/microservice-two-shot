import React from 'react'
import { Link } from 'react-router-dom'

const deleted = async (id) => {
    fetch(`http://localhost:8090/api/hats/${id}`, {
       method: "DELETE",
       headers: {
           'Content-Type': 'application/json'
       }
  });
    window.location.reload()

}


function HatCol(props){

    return (
        <div className="col">
            {props.list.map(data => {
                const hats = data;

                console.log('data:', hats.id);
                return (

                        <div key = {hats.id} className="card mb-3 shadow">
                            <img src={hats.hat_url} className="card-img-top" />


                          <div className="card-body">
                            <h3 className="card-Title">{hats.style_type} {hats.fabric}</h3>
                            <h4 className="card-subtitle mb-2 text-muted">
                                color: {hats.color}
                            </h4>
                            <h4 className="card-subtitle mb-2 text-muted">
                                location: {hats.location.room_section}
                            </h4>

                          </div>
                          <div className="card-footer">
                            <button onClick={() => deleted(hats.id)} type="button">
                                Delete
                            </button>

                          </div>

                        </div>

                );
            })}
        </div>
    )
}


class ListHats extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            hatCols: [[], [], []],
        };


    }


    async componentDidMount() {
        const url = "http://localhost:8090/api/hats/"
        try {
            const response = await fetch(url)

            if (response.ok) {
                const data = await response.json()

                const requests = []
                for (let hat of data.hatslist) {
                    const detailUrl = `http://localhost:8090/api/hats/${ hat.id }`;
                    requests.push(fetch(detailUrl))
                }
                const responses = await Promise.all(requests)

                const hatCols = [[], [], []]
                let i = 0
                for (const hatResp of responses){
                    if (hatResp.ok) {
                        const details = await hatResp.json()
                        hatCols[i].push(details);
                        i = i +1
                        if (i > 2) {
                            i = 0
                        }
                    } else {
                        console.error(hatResp)
                    }
                }
                this.setState({hatCols: hatCols})

            }
        } catch (e) {
            console.error(e)
        }
    }

    render() {
        return (
            <>
                <div className="px-4 py-5 m-5 mt-0 text-center bg-info">
                    <img className="bg-white rounded shadow d-block mx-auto mb-4" src="" alt=""/>
                    <h1 className="display-5">Wardrobe Hats!</h1>
                    <div className= "col-lg-6 mx-auto">
                        <p className= "lead mb-4">
                            Plan your outfit Starting with Your Hat!!
                        </p>
                        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                            <Link to="/hats/new" className='btn btn-primary'>Create Hats</Link>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <h2>Hats, Hats, and more Hats</h2>
                    <div className="row">
                        {this.state.hatCols.map((listHat, index) => {
                            return (
                                <HatCol key={index} list={listHat}  />
                            )
                        })}
                    </div>
                </div>
            </>
        )
    }
}
export default ListHats;
