import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.
# from shoes_rest.models import Something

from shoes_rest.models import BinVO


def getBin():
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    content = json.loads(response.content)
    print(content)
    for bin in content["bins"]:
        try:
            obj, created = BinVO.objects.update_or_create(
                import_href=bin["href"],
                defaults={"name": bin["closet_name"]}
            )
            if created:
                print("Created binVO object:", obj)

            else:
                print("Updated object:", obj)

        except Exception as e:
            print("Got an error:", e)




def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            getBin()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()

