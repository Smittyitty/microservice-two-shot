from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Shoe(models.Model):
    """
    The Shoes model!
    """
    name = models.CharField(max_length= 150)
    color = models.CharField(max_length=20)
    manufacturer = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)


    bin = models.ForeignKey(
        BinVO,
        related_name="bin",
        on_delete=models.PROTECT,
        null=True,
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_shoe_detail", kwargs={"pk": self.pk})

