from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

from .models import Shoe, BinVO

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["name", "import_href"]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["name",
                  "manufacturer",
                  "id",
                  "color",
                  "picture_url",
                  "bin",
                  ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["name",
                  "color",
                  "manufacturer",
                  "picture_url",
                  "bin"
                  ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


@require_http_methods(["GET","POST"])
def ShoeList(request):
    
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes":shoes},
            encoder=ShoeListEncoder,
            )
    else:
        content = json.loads(request.body)

        try:
            bin_href = f"/api/bins/{content['bin']}/"
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin

        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Invalid bin id"}, status=400)

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
@require_http_methods(["GET","POST","DELETE"])
def ShoeDetail(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder = ShoeDetailEncoder,
            safe=False
        )

    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

