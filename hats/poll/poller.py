import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
# from hats_rest.models import Something
from hats_rest.models import LocationVo


def locate():
    response = requests.get("http://wardrobe-api:8000/api/locations")
    content = json.loads(response.content)
    print('inFUNCTION', content)
    for location in content['locations']:
        print('in LOOOP', location)
        LocationVo.objects.update_or_create(
            import_href=location['href'],
            defaults={
                "room_name": location['closet_name'],
                "room_section": location["section_number"],
                "hook_num": location['shelf_number'],
            }
        )


def poll():
    while True:
        print('Hats poller polling for data')
        try:
            print('HERRRE')
            locate()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
