from django.db import models


class LocationVo(models.Model):
    import_href = models.CharField(max_length=200, null=True, unique=True)
    room_name = models.CharField(max_length=200)
    room_section = models.PositiveIntegerField(null=True)
    hook_num = models.PositiveIntegerField(null=True)

    def __str__(self):
        return f"name: {self.room_name}, section: {self.room_section}, hook: {self.hook_num}"


class Hats(models.Model):
    brand = models.CharField(max_length=200)
    color = models.CharField(max_length=100, default=None)
    style_type = models.CharField(max_length=250)
    fabric = models.CharField(max_length=100, default=None)
    hat_url = models.URLField(default=None)
    location = models.ForeignKey(
        LocationVo,
        related_name="hatslist",
        on_delete=models.CASCADE,
        default=None

    )
