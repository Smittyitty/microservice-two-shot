from django.http import JsonResponse
import json
from .models import LocationVo, Hats
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods


class LocationVoDetailEncoder(ModelEncoder):
    model = LocationVo
    properties = [
        'import_href',
        'room_name',
        'room_section',
        'hook_num',
    ]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "brand",
        "style_type",
        "id",
        "hat_url",
        "location",
    ]
    encoders = {
        'location':  LocationVoDetailEncoder()
    }


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "brand",
        "color",
        "style_type",
        "fabric",
        "hat_url",
        "location",
    ]
    encoders = {
        "location": LocationVoDetailEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_hats_list(request):
    if request.method == "GET":
        hatslist = Hats.objects.all()
        return JsonResponse(
            {
                "hatslist": hatslist
            },
            encoder=HatsListEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            href_loc = content['location']
            location = LocationVo.objects.get(import_href=href_loc)
            content["location"] = location
        except LocationVo.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hatslist = Hats.objects.create(**content)
        return JsonResponse(
            hatslist,
            encoder=HatsDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET"])
def hats_details(request, id):
    if request.method == "GET":
        hat = Hats.objects.get(id=id)
        return JsonResponse(
           hat,
           encoder=HatsDetailEncoder,
           safe=False
        )
    else:
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
