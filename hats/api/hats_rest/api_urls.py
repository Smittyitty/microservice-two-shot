from .api_views import api_hats_list, hats_details
from django.urls import path


urlpatterns = [
    path("hats/", api_hats_list, name="hats_list"),
    path("hats/<int:id>/", hats_details, name="hats_details"),
]
