from django.contrib import admin
from .models import LocationVo, Hats
# Register your models here.

@admin.register(Hats)
class HatsAdmin(admin.ModelAdmin):
    pass

@admin.register(LocationVo)
class LocationVoAdmin(admin.ModelAdmin):
    pass
