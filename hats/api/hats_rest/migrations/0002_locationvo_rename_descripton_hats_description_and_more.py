# Generated by Django 4.0.3 on 2023-07-19 19:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LocationVo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('import_href', models.CharField(max_length=200, null=True, unique=True)),
                ('room_name', models.CharField(max_length=200)),
                ('room_section', models.PositiveIntegerField(null=True)),
                ('hook_num', models.PositiveIntegerField(null=True)),
            ],
        ),
        migrations.RenameField(
            model_name='hats',
            old_name='descripton',
            new_name='description',
        ),
        migrations.RenameField(
            model_name='hats',
            old_name='style',
            new_name='style_type',
        ),
        migrations.AddField(
            model_name='hats',
            name='location',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='hatslist', to='hats_rest.locationvo'),
        ),
    ]
