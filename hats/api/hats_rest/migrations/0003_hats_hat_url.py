# Generated by Django 4.0.3 on 2023-07-20 14:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_locationvo_rename_descripton_hats_description_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='hats',
            name='hat_url',
            field=models.URLField(default=None),
        ),
    ]
